# Multiplikasjon av 2 matriser med numpy
import numpy as np

a = np.array([[1,2,3,4,5],[3,4,5,6,7],[2,2,3,4,4]])
b = np.array([[2,4,8],[6,8,10],[1,2,3],[2,3,4],[4,3,2]])
if a.shape[1] == b.shape[0]:
    print(a@b)
else:
    print('Det går dessverre ikke.')