import matplotlib
import matplotlib.pyplot as plt
import numpy as np # Data for plotting
t = np.arange(-6.0, 4.5, 0.01)
# t blir vektoren [0.00, 0.01, 0.02, 0.03,...1.99]
s = t**3 + 2*t**2 - 20*t + 1
# her regnes 2*pi*t + 1 for hver verdi av t og
# dette legges inn i vektoren s
fig, ax = plt.subplots()
# genererer grafen og returnerer en referanse til figuren (til fig)
# og en liste over aksene (ax)

ax.plot(t, s) # plotter selve grafen

ax.set(xlabel='x', ylabel='y',
       title='x^3 + 2x^2 - 20x + 1')
# setter navn på aksene

ax.grid() # tegner en grid på grafen
fig.savefig("test.png") # Lagrer grafen som en png-fil
plt.show() # Viser vinduet med grafen 

