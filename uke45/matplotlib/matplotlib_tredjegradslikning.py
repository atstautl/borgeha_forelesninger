import matplotlib
import matplotlib.pyplot as plt
import numpy as np # Data for plotting

t = np.arange(-6.0, 4.5, 0.01)

s = t**3 + 2*t**2 - 20*t + 1

# her beregnes funksjonen over for hver verdi av t og
# dette legges inn i vektoren s

fig, ax = plt.subplots()
# genererer grafen og returnerer en referanse til figuren (til fig)
# og en liste over aksene (ax)

ax.plot(t, s) # plotter selve grafen

ax.set(xlabel='x', ylabel='y',
       title='x^3 + 2x^2 - 20x + 1')
# setter navn på aksene

# En intrikat måte å sette fontstørrelsen... kan sikkert forenkles
for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
             ax.get_xticklabels() + ax.get_yticklabels()):
    item.set_fontsize(120)

ax.grid() # tegner en grid på grafen
fig.savefig("tredjegrad.png") # Lagrer grafen som en png-fil
plt.show() # Viser vinduet med grafen
